#include <cecs.h>

typedef struct Vec2 {
	float x;
	float y;
} Vec2;

typedef Vec2 Position;
typedef Vec2 Velocity;

void world_new(void)
{
	ecs_world_t* world = ecs_init();

	ecs_fini(world);
	ecs_os_log("world_new passed");
}

void world_add_component(void)
{
	ecs_world_t* world = ecs_init();

	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	assert(ecs_component_is_known(Position) == true);
	assert(ecs_component_is_known(Velocity) == true);
	assert(ecs_component_is_known(uint32_t) == false);

	ecs_fini(world);
	ecs_os_log("world_add_component passed");
}

void world_add_entity(void)
{
	ecs_world_t* world = ecs_init();

	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	ECS_ENTITY(world, Entity, Position, Velocity);
	ECS_ENTITY(world, Entity2, Velocity, Position);
	ECS_ENTITY(world, Entity3, uint32_t);
	assert(Entity3 == ECS_INVALID_ENTITY_HANDLE);

	assert(Entity != ECS_INVALID_ENTITY_HANDLE);
	assert(Entity2 != ECS_INVALID_ENTITY_HANDLE);

	assert(ecs_entity_lookup(world, Entity) == true);
	assert(ecs_entity_lookup(world, Entity2) == true);
	assert(ecs_entity_lookup(world, 10) == false);

	assert(Entity != Entity2);

	ecs_fini(world);
	ecs_os_log("world_add_entity passed");
}

void world_get_component(void) 
{
	ecs_world_t* world = ecs_init();

	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	ECS_ENTITY(world, Entity, Position, Velocity);
	ECS_ENTITY(world, Entity2, Velocity, Position);

	ECS_GET_COMPONENT(world, Entity, Position, p1);
	ECS_GET_COMPONENT(world, Entity2, Position, p2);
	ECS_GET_COMPONENT(world, 3, Position, p3);

	assert(p1 != NULL);
	assert(p2 != NULL);
	assert(p3 == NULL);

	ECS_SET(world, Entity, Position, { .x = 10, .y = 10 });

	// These should have no effect
	ECS_SET(world, 5, Position, { .x = 10, .y = 10 });
	ECS_SET(world, 3, Velocity, { .x = 10, .y = 10 });

	assert(p1->x == 10);
	assert(p1->y == 10);

	assert(p1 != p2);
	// Ensure that they are continuous in memory
	assert((uintptr_t)p2 - (uintptr_t)p1 == sizeof(Position));

	ecs_fini(world);
	ecs_os_log("world_get_component passed");
}

void world_get_all_components(void)
{
	ecs_world_t* world = ecs_init();

	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	ECS_ENTITY(world, Entity, Position, Velocity);
	ECS_ENTITY(world, Entity2, Velocity, Position);

	ECS_GET_COMPONENT(world, Entity, Position, p1);
	ECS_GET_COMPONENT(world, Entity2, Position, p2);

	// Get all position components from the Position,Velocity archetype
	ECS_COLUMN(world, Position, p_arr, Position, Velocity);
	assert(p_arr != NULL);

	// Make sure the array points to the right elements
	assert(p1 == p_arr);
	assert(p2 == p_arr + 1);

	ECS_ENTITY(world, Entity3, Position);
	ECS_ENTITY(world, Entity4, Position);
	ECS_ENTITY(world, Entity5, Position);

	ECS_COLUMN(world, Position, p_only_arr, Position);
	assert(p_only_arr != NULL);
	assert(p1 != p_only_arr); 

	ecs_fini(world);
	ecs_os_log("world_get_all_components passed");
}

void Move(ecs_world_t* world, float deltaTime)
{
	ECS_COLUMN(world, Position, p, Position, Velocity);
	ECS_COLUMN(world, Velocity, v, Position, Velocity);

	for (uint32_t i = 0; i < p_count; ++i)
	{
		p[i].x += v[i].x * deltaTime;
		p[i].y += v[i].y * deltaTime;
	}
}

void world_add_system(void)
{
	ecs_world_t* world = ecs_init();

	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	ECS_ENTITY(world, Entity, Position, Velocity);
	ECS_ENTITY(world, Entity2, Velocity, Position);

	ECS_SYSTEM(world, Move, EcsOnUpdate);

	ecs_fini(world);
	ecs_os_log("world_add_system passed");
}

void world_run_system(void)
{
	ecs_world_t* world = ecs_init();

	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	ECS_ENTITY(world, Entity, Position, Velocity);
	ECS_ENTITY(world, Entity2, Velocity, Position);

	ECS_SYSTEM(world, Move, EcsOnUpdate);

	ECS_GET_COMPONENT(world, Entity, Position, p1);
	ECS_GET_COMPONENT(world, Entity2, Position, p2);

	p1->x = 0;
	p1->y = 0;
	p2->x = 0;
	p2->y = 0;

	ECS_GET_COMPONENT(world, Entity, Velocity, v1);
	ECS_GET_COMPONENT(world, Entity2, Velocity, v2);

	v1->x = 10;
	v1->y = 10;
	v2->x = -10;
	v2->y = -10;

	ecs_world_update(world, 1.0);

	assert(p1->x == 10.0);
	assert(p1->y == 10.0);
	assert(p2->x == -10.0);
	assert(p2->y == -10.0);

	ecs_fini(world);
	ecs_os_log("world_run_system passed");
}