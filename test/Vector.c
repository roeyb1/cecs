#include <cecs.h>

void vector_add(void)
{
	ecs_vector_params_t params = { .element_size = sizeof(uint32_t) };
	ecs_vector_t* vector = ecs_vector_new(&params, 10);
	assert(vector != NULL);

	// Initialize the vector to some known values
	for (uint32_t i = 0; i < 10; i++)
	{
		uint32_t* a = ecs_vector_add(&vector, &params);
		*a = 0x10 + i;
	}

	// This will overflow the vector and cause it to resize:
	uint32_t a = 0;
	ecs_vector_addi(&vector, &params, &a);

	assert(a == 10);

	ecs_vector_free(vector);
	ecs_os_log("vector_add passed");
}

void vector_addn(void)
{
	ecs_vector_params_t params = { .element_size = sizeof(uint32_t) };
	ecs_vector_t* vector = ecs_vector_new(&params, 10);
	assert(vector != NULL);

	uint32_t* a = ecs_vector_addn(&vector, &params, 10);

	assert(ecs_vector_get_index(vector, &params, a) == 0);

	// Initialize the vector to some known values
	for (uint32_t i = 0; i < 10; i++)
	{
		a[i] = 0xA0 + i;
	}

	// Overflow the vector by attempting to add 10 more elements
	ecs_vector_addn(&vector, &params, 10);
	assert(ecs_vector_size(vector) == 20);
	assert(ecs_vector_count(vector) == 20);

	a = ecs_vector_first(vector);
	a += 5;
	assert(ecs_vector_get_index(vector, &params, a) == 5);

	// Try adding to NULL
	ecs_vector_t* null_vector = NULL;
	ecs_vector_add(&null_vector, &params);
	assert(ecs_vector_size(null_vector) == 1);

	ecs_vector_free(null_vector);
	null_vector = NULL;
	ecs_vector_addn(&null_vector, &params, 10);
	assert(ecs_vector_size(null_vector) >= 10);
	
	ecs_vector_free(vector);
	ecs_vector_free(null_vector);

	ecs_vector_t* empty_vector = ecs_vector_new(&params, 0);
	a = ecs_vector_add(&empty_vector, &params);
	assert(a != NULL);
	*a = 10;
	a = ecs_vector_add(&empty_vector, &params);
	assert(ecs_vector_count(empty_vector) == 2);
	assert(ecs_vector_size(empty_vector) != 0);

	ecs_vector_free(empty_vector);
	ecs_os_log("vector_addn passed");
}

void vector_remove(void)
{
	ecs_vector_params_t params = {
		.element_size = sizeof(uint32_t),
		.move_flag = ECS_VECTOR_REMOVE_ORDERED };
	ecs_vector_t* vector = ecs_vector_new(&params, 10);

	uint32_t* a = ecs_vector_addn(&vector, &params, 10);
	for (uint32_t i = 0; i < 10; i++)
	{
		a[i] = 0xB0 + i;
	}

	// Test ordered vector remove
	ecs_vector_remove(vector, &params, 4);
	assert(a[4] == 0xB5);
	assert(ecs_vector_count(vector) == 9);
	assert(ecs_vector_size(vector) == 10);
	
	// Test unordered vector remove
	params.move_flag = ECS_VECTOR_REMOVE_SWAP_LAST;
	ecs_vector_remove(vector, &params, 4);
	assert(a[4] == 0xB9);
	assert(*(uint32_t*)ecs_vector_last(vector, &params) == 0xB8);
	assert(a[5] == 0xB6);
	assert(ecs_vector_count(vector) == 8);
	assert(ecs_vector_size(vector) == 10);

	ecs_vector_free(vector);
	ecs_os_log("vector_remove passed");
}

void vector_get(void)
{
	ecs_vector_params_t params = { .element_size = sizeof(uint32_t) };
	ecs_vector_t* vector = ecs_vector_new(&params, 10);
	assert(vector != NULL);


	uint32_t* a = ecs_vector_add(&vector, &params);
	assert(a == ecs_vector_first(vector));
	assert(a == ecs_vector_get(vector, &params, 0));
	assert(ecs_vector_get(vector, &params, 1) == NULL);

	ecs_vector_clear(vector);
	ecs_vector_addn(&vector, &params, 5);
	a = ecs_vector_get(vector, &params , 3);
	// THIS TEST WILL FAIL IF ECS_VECTOR_T IS CHANGED
	// 8 is the size of ecs_vector_t. 
	assert(a == (uint32_t*)((uintptr_t)vector + 8 + 3 * sizeof(uint32_t)));
	assert(a != ecs_vector_last(vector, &params));
	
	a = ecs_vector_get(vector, &params, 10);
	assert(a == NULL);
	a = ecs_vector_get(vector, &params, 4);
	assert(a == ecs_vector_last(vector, &params));

	ecs_vector_free(vector);
	ecs_os_log("vector_get passed");
}

void vector_clear(void)
{
	ecs_vector_params_t params = { .element_size = sizeof(uint32_t) };
	ecs_vector_t* vector = ecs_vector_new(&params, 10);
	assert(vector != NULL);

	uint32_t* a = ecs_vector_addn(&vector, &params, 10);

	// Initialize the vector to some known values
	for (uint32_t i = 0; i < 10; i++)
	{
		a[i] = 0xB0 + i;
	}

	ecs_vector_clear(vector);

	a = ecs_vector_addn(&vector, &params, 2);

	assert(a[0] == 0xB0);
	assert(a[1] == 0xB1);
	ecs_vector_free(vector);
	ecs_os_log("vector_clear passed");
}