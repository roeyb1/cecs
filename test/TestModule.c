#include <cecs.h>

#include "Module.h"

void module_import(void)
{
	ecs_world_t* world = ecs_init();

	ECS_IMPORT(world, TestModule);

	ECS_ENTITY(world, Entity, Position, Velocity);
	assert(ecs_entity_lookup(world, Entity) == true);

	ECS_SET(world, Entity, Position, { .x = 0, .y = 0 });
	ECS_SET(world, Entity, Velocity, { .x = 10, .y = 10 });

	ecs_world_update(world, 1.0);

	ECS_GET_COMPONENT(world, Entity, Position, p);
	assert(p->x == 10);
	assert(p->y == 10);

	ecs_fini(world);
	ecs_os_log("module_import passed");
}