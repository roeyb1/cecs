#include <cecs.h>

#include <string.h>

void map_new()
{
	ecs_map_t* map = ecs_map_new(8, sizeof(char*));
	assert(map != NULL);

	assert(ecs_map_data_size(map) == sizeof(char*));

	assert(ecs_map_count(map) == 0);
	assert(ecs_map_bucket_count(map) == 8);

	ecs_os_log("map_new passed");
	ecs_map_free(map);
}

void map_set()
{
	ecs_map_t* map = ecs_map_new(8, sizeof(char*));
	assert(map != NULL);

	char* test_str1 = "Hello";
	char* test_str2 = "world";
	char* test_str3 = "Hello world!";

	assert(ecs_map_count(map) == 0);
	ecs_map_set(map, 1, &test_str1);
	assert(ecs_map_count(map) == 1);
	ecs_map_set(map, 2, &test_str2);
	assert(ecs_map_count(map) == 2);
	ecs_map_set(map, 3, &test_str3);
	assert(ecs_map_count(map) == 3);

	char* buff = NULL;
	assert(ecs_map_has(map, 1, &buff) == true);
	assert(strcmp(buff, "Hello") == 0);
	assert(ecs_map_has(map, 2, &buff) == true);
	assert(strcmp(buff, "world") == 0);
	assert(ecs_map_has(map, 3, &buff) == true);
	assert(strcmp(buff, "Hello world!") == 0);
	assert(ecs_map_has(map, 4, &buff) == false);

	assert(ecs_map_count(map) == 3);

	char* test = NULL;
	ecs_map_set(map, 1, &test);
	assert(ecs_map_has(map, 1, &buff) == true);
	assert(buff == NULL);

	// By now the map has 3 entries, it should resize when load factor is exceeded
	ecs_map_set(map, 4, &test_str1);
	ecs_map_set(map, 5, &test_str1);

	test_str1 = "6";
	ecs_map_set(map, 6, &test_str1);
	ecs_map_set(map, 7, &test_str1);
	test_str1 = "8";
	ecs_map_set(map, 8, &test_str1);

	assert(ecs_map_has(map, 6, &buff) == true);
	assert(strcmp(buff, "6") == 0);
	assert(ecs_map_has(map, 8, &buff) == true);
	assert(strcmp(buff, "8") == 0);

	assert(ecs_map_count(map) == 8);

	ecs_map_set(map, 3, &test_str3);
	assert(ecs_map_has(map, 3, &buff) == true);
	assert(strcmp(buff, "Hello world!") == 0);

	char** test_ptr = ecs_map_get_data_ptr(map, 3);
	assert(strcmp(*test_ptr, "Hello world!") == 0);
	//test_str1 = "Change";
	assert(ecs_map_has(map, 3, &buff) == true);
	assert(strcmp(*test_ptr, buff) == 0);

	ecs_os_log("map_set passed");
	ecs_map_free(map);
}

void map_remove()
{
	ecs_map_t* map = ecs_map_new(8, sizeof(char*));
	assert(map != NULL);

	char* test_str1 = "One";
	char* test_str2 = "Two";
	char* test_str3 = "Three";
	char* test_str4 = "Four";

	assert(ecs_map_count(map) == 0);
	ecs_map_set(map, 1, &test_str1);
	assert(ecs_map_count(map) == 1);
	ecs_map_set(map, 2, &test_str2);
	assert(ecs_map_count(map) == 2);
	ecs_map_set(map, 3, &test_str3);
	assert(ecs_map_count(map) == 3);
	ecs_map_set(map, 4, &test_str4);
	assert(ecs_map_count(map) == 4);

	assert(ecs_map_remove(map, 2) == true);
	
	char* buff;
	assert(ecs_map_has(map, 1, &buff) == true);
	assert(strcmp(buff, "One") == 0);
	assert(ecs_map_has(map, 3, &buff) == true);
	assert(strcmp(buff, "Three") == 0);
	assert(ecs_map_has(map, 4, &buff) == true);
	assert(strcmp(buff, "Four") == 0);
	assert(ecs_map_has(map, 2, &buff) == false);

	char* test_str5 = "Five";
	ecs_map_set(map, 5, &test_str5);
	assert(ecs_map_has(map, 5, &buff) == true);
	assert(strcmp(buff, "Five") == 0);

	ecs_os_log("map_remove passed");
	ecs_map_free(map);
}

void map_iter(void)
{
	ecs_map_t* map = ecs_map_new(8, sizeof(char*));
	assert(map != NULL);

	char* test_str1 = "One";
	char* test_str2 = "Two";
	char* test_str3 = "Three";
	char* test_str4 = "Four";

	assert(ecs_map_count(map) == 0);
	ecs_map_set(map, 1, &test_str1);
	assert(ecs_map_count(map) == 1);
	ecs_map_set(map, 2, &test_str2);
	assert(ecs_map_count(map) == 2);
	ecs_map_set(map, 3, &test_str3);
	assert(ecs_map_count(map) == 3);
	ecs_map_set(map, 4, &test_str4);
	assert(ecs_map_count(map) == 4);

	ecs_map_iter_t iter = ecs_map_get_iter(map);

	assert(ecs_map_has_next(&iter));
	assert(strcmp(*(char**)ecs_map_next(&iter), "One") == 0);

	assert(ecs_map_has_next(&iter));
	assert(strcmp(*(char**)ecs_map_next(&iter), "Two") == 0);

	assert(ecs_map_has_next(&iter));
	assert(strcmp(*(char**)ecs_map_next(&iter), "Three") == 0);

	assert(ecs_map_has_next(&iter));
	assert(strcmp(*(char**)ecs_map_next(&iter), "Four") == 0);

	assert(ecs_map_has_next(&iter) == false);

	ecs_os_log("map_iter passed");
	ecs_map_free(map);
}

void map_edge(void)
{
	ecs_map_t* map = ecs_map_new(8, sizeof(char*));
	assert(map != NULL);

	char* test_str1 = "One";
	char* test_str2 = "Two";
	char* test_str3 = "Three";

	assert(ecs_map_count(map) == 0);
	ecs_map_set(map, 1, &test_str1);
	ecs_map_set(map, 9, &test_str2);

	assert(ecs_map_remove(map, 2) == false);

	char* buff;
	assert(ecs_map_has(map, 9, &buff) == true);
	assert(strcmp(buff, "Two") == 0);

	assert(ecs_map_remove(map, 1) == true);
	assert(ecs_map_count(map) == 1);

	assert(ecs_map_has(map, 9, &buff) == true);
	assert(strcmp(buff, "Two") == 0);

	ecs_map_set(map, 1, &test_str1);
	ecs_map_set(map, 17, &test_str3);
	ecs_map_set(map, 18, &test_str1);

	assert(ecs_map_remove(map, 1) == true);
	assert(ecs_map_has(map, 9, &buff) == true);
	assert(strcmp(buff, "Two") == 0);
	assert(ecs_map_has(map, 17, &buff) == true);
	assert(strcmp(buff, "Three") == 0);

	assert(ecs_map_has(map, 18, &buff) == true);
	assert(strcmp(buff, "One") == 0);
	assert(ecs_map_has(map, 1, &buff) == false);

	ecs_map_set(map, 1, &test_str1);
	assert(ecs_map_remove(map, 9) == true);

	assert(ecs_map_get_data_ptr(map, 2) == NULL);

	ecs_os_log("map_edge passed");
	ecs_map_free(map);
}