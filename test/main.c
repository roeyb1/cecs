#include <cecs.h>

// Log test suite
void debug_mode(void);
void logs(void);
void asserts(void);

// Vector test suite
void vector_add(void);
void vector_addn(void);
void vector_remove(void);
void vector_get(void);
void vector_clear(void);

// Map test suite
void map_new(void);
void map_set(void);
void map_remove(void);
void map_iter(void);
void map_edge(void);

// World test suite
void world_new(void);
void world_add_component(void);
void world_add_entity(void);
void world_get_component(void);
void world_get_all_components(void);
void world_add_system(void);
void world_run_system(void);

// Module test suite
void module_import(void);

int main()
{
	ecs_os_set_api_defaults();

	ecs_os_log("--- Starting log suite... ---");
	debug_mode();
	logs();
	asserts();
	
	ecs_os_log("--- Starting vector suite... ---");
	vector_add();
	vector_addn();
	vector_remove();
	vector_get();
	vector_clear();

	ecs_os_log("--- Starting map suite... ---");
	map_new();
	map_set();
	map_remove();
	map_iter();
	map_edge();

	ecs_os_log("--- Starting world suite... ---");
	world_new();
	world_add_component();
	world_add_entity();
	world_get_component();
	world_get_all_components();
	world_add_system();
	world_run_system();

	ecs_os_log("--- Starting module suite... ---");
	module_import();

	ecs_os_log("--- All tests passed. ---");
	return 0;
}