#ifndef TEST_MODULE_H
#define TEST_MODULE_H

typedef struct Vec2 {
	float x;
	float y;
} Vec2;

// Some test components
typedef Vec2 Position;
typedef Vec2 Velocity;

// A test sys
void MoveModuleSys(ecs_world_t* world, float dt);

// What gets called automatically when ECS_IMPORT is called
void TestModuleImport(ecs_world_t* world);


#endif //TEST_MODULE_H