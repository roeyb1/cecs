#include <cecs.h>

void debug_mode()
{
	ecs_os_enable_dbg(true);
	assert(ecs_os_dbg_enabled() == true);
}

void logs()
{
	ecs_os_log("Test log");
	ecs_os_warn("Test warning");
	ecs_os_err("Test error");
	ecs_os_dbg("Test debug");
}

void test_abort(void)
{
	return;
}

void asserts()
{
	// Disable abort() so that program doesn't terminate on failed assertions.
	ecs_os_api_t ecs_api;
	ecs_api = ecs_os_api;
	ecs_api.abort = test_abort;
	ecs_os_set_api(&ecs_api);
	
	uint32_t a = 10;
	ecs_assert(1 == 0, ECS_OUT_OF_MEMORY, 0);
	ecs_assert(a != 10, ECS_INVALID_PARAM, "a");
	ecs_assert(1 == 0, ECS_INTERNAL_ERROR, 0);
	ecs_assert(1 == 0, -1, NULL);

	ecs_os_set_api_defaults(); // will simply return since api has already been initialized.
}