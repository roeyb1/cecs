#include <cecs.h>
#include "Module.h"

void MoveModuleSys(ecs_world_t* world, float dt)
{
	ECS_COLUMN(world, Position, p, Position, Velocity);
	ECS_COLUMN(world, Velocity, v, Position, Velocity);

	for (uint32_t i = 0; i < p_count; ++i)
	{
		p[i].x += v[i].x * dt;
		p[i].y += v[i].y * dt;
	}
}

void TestModuleImport(ecs_world_t* world)
{
	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);

	ECS_SYSTEM(world, MoveModuleSys, EcsOnUpdate);
}