cmake_minimum_required(VERSION 3.0)

project (Arcane-Engine C)
set(CMAKE_C_STANDARD 11)

file (GLOB cecs_SRC "src/*.c")

set(EXAMPLES OFF CACHE BOOL "Examples")
set(DEBUG OFF CACHE BOOL "Debug")
set(TESTING OFF CACHE BOOL "Test")
set(COVERAGE OFF CACHE BOOL "Coverage")

if(DEBUG)
    set(CMAKE_BUILD_TYPE Debug)
else()
    set(CMAKE_BUILD_TYPE Release)
endif()

include_directories("include")

add_library(cecs_static STATIC ${cecs_SRC})

if(DEBUG)
    target_compile_definitions(cecs_static PUBLIC DEBUG)
endif()

if(COVERAGE)
    target_compile_options(cecs_static PRIVATE -coverage)
    target_link_libraries(cecs_static PRIVATE gcov)
endif()

if (EXAMPLES)
    add_subdirectory("examples/bounce")
endif()

if (TESTING)
    enable_testing()
    add_subdirectory(test)
endif()