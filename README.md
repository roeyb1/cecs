[![pipeline status](https://gitlab.com/roeyb1/cecs/badges/master/pipeline.svg)](https://gitlab.com/roeyb1/cecs/commits/master) [![codecov](https://codecov.io/gl/roeyb1/cecs/branch/master/graph/badge.svg)](https://codecov.io/gl/roeyb1/cecs)

# C Entity-Component-System

CECS is a lightweight **entity component system** framework written in C with no dependencies for high performance simulations or game engine development. It leverages Data-Oriented-Design over Object-Oriented principles to achieve high performance.

**Current Features**:
- High performance sequential containers (vectors, maps)
- Modular OS API functions for multiplatform support
- Optimized internal structures to encourage CPU caching.
- Module support

**Future**:
- Multithreading


# Introduction

Details about the ECS architecture:
- [Entity Systems Wiki](http://entity-systems.wikidot.com/)
- [ECS on Wikipedia](https://en.wikipedia.org/wiki/Entity_component_system)

# The storage of data within CECS
In order to make operations on data (components) fast, data with temporal locality should always be stored continuously in memory with as few gaps as possible. This is to optimize CPU caching. 

In a typical game engine or simulation, data of the same type is often accessed at the same time. For example updating the position of all moveable objects in an engine. The engine will iterate over the position and velocity of each object for each one and apply some operation. Thus by storing each in a sequential array, when cache lines are retrieved, it is more likely that it will also contain the next position and the next component while iterating.

**CECS Core Table Structure:**

An archetype is a list of component types associated with a given entity. For example, all entities with a Position and Velocity component would be considered of the same archetype. Tables are stored in the engine for each archetype and thus make operations on all components of the same archetype simple.

| Entity | Comp. A | Comp. B | Comp. C | Comp. D |
|--------|:-------:|:-------:|:-------:|:-------:|
| 1      | X       |         | X       |         |
| 2      | X       | X       |         | X       |
| 3      | X       |         | X       |         |
| 4      | X       | X       |         |         | 

where each X here represents an index into a continuous vector of components of the corresponding type.

Packed data would look something like this:

| Component Type | Data |   |   |   |
|----------------|-----:|--:|---|---|
| Comp. A        |    1 | 3 | 2 | 4 |
| Comp. B        |    2 | 4 |   |   |
| Comp. C        |    3 | 1 |   |   |
| Comp. D        |    2 |   |   |   |
