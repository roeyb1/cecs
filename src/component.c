#include "cecs_private.h"

static ecs_vector_t* known_types = NULL;
static ecs_vector_params_t known_types_params = { .element_size = sizeof(ecs_component_info_t) };

ecs_component_info_t* ecs_get_component_info(
	ecs_world_t* world,
	ecs_archetype_t* archetype,
	uint32_t index)
{
	if (known_types == NULL) return NULL;

	ecs_component_info_t* arr = ecs_vector_first(known_types);
	const char* id = *(char**)ecs_vector_get(archetype->types, &archetype_id_params, index);

	for (uint32_t i = 0; i < ecs_vector_count(known_types); ++i)
	{
		if (strcmp(arr[i].id, id) == 0)
		{
			return &(arr[i]);
		}
	}
	return NULL;
}

bool _ecs_component_is_known(
	const char* id)
{
	uint32_t count = ecs_vector_count(known_types);
	ecs_component_info_t* arr = ecs_vector_first(known_types);

	for (uint32_t i = 0; i < count; ++i)
	{
		if (strcmp(arr[i].id, id) == 0)
			return true;
	}
	return false;
}

void ecs_new_component(
	ecs_world_t* world,
	const char* id,
	size_t size)
{
	// If this is the first component, initialize the array
	if (known_types == NULL)
	{
		known_types = ecs_vector_new(&known_types_params, 0);
	}

	ecs_component_info_t* info = (ecs_component_info_t*)ecs_vector_add(&known_types, &known_types_params);
	info->id = id;
	info->size = size;
}

void ecs_reset_known_components()
{
	if (known_types != NULL)
		ecs_vector_free(known_types);
	known_types = NULL;
}