#include "cecs_private.h"

void ecs_new_system(
	ecs_world_t* world,
	ecs_system_function_t func,
	EcsSystemType type)
{
	EcsSystem* result = ecs_os_malloc(sizeof(EcsSystem));
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);

	result->function = func;
	result->type = type;
	
	EcsSystem** ptr = NULL;
	switch (result->type)
	{
	case EcsOnUpdate:
		ptr = ecs_vector_add(&(world->on_update_systems), &handle_params);
		break;
	case EcsOnQuit:
		ptr = ecs_vector_add(&(world->on_quit_systems), &handle_params);
	}
	*ptr = result;
}