#include "cecs_private.h"

#define MAP_LOAD_FACTOR (3.0f / 4.0f)
#define MAP_INITIAL_NODE_COUNT (4)

typedef struct ecs_map_node_t
{
	uint32_t next;	/* Next node index */
	uint32_t prev;	/* Previous node index */
	uint64_t key;	/* Key */
} ecs_map_node_t;

typedef uint32_t bucket_t;

struct ecs_map_t
{
	uint32_t bucket_count;	/* Number of bucket */
	uint32_t count;			/* Number of elements*/
	bucket_t* buckets;		/* Array of buckets */
	ecs_vector_t* nodes;	/* Vector of nodes */
	ecs_vector_params_t node_params;	/* Params for node vector */
};

static size_t data_size(
	ecs_map_t* map)
{
	return map->node_params.element_size - sizeof(ecs_map_node_t);
}

static void allocate_buckets(
	ecs_map_t* map,
	uint32_t num_buckets)
{
	if (map == NULL) return;
	if (!num_buckets) return;

	// Initialize everything here to 1 so that it can be checked to be invalid with
	// if (!bucket) which would evaluate to true.
	map->buckets = ecs_os_calloc(num_buckets * sizeof(bucket_t), 1);
	ecs_assert(map->buckets != NULL, ECS_OUT_OF_MEMORY, 0);

	map->bucket_count = num_buckets;
}

/* Return a pointer to the bucket given a key */
static bucket_t* get_bucket(
	ecs_map_t* map,
	uint64_t key)
{
	// This index is guaranteed to be in range.
	uint64_t index = key % map->bucket_count;
	return &(map->buckets[index]);
}

/* Gets a node from a given index */
static ecs_map_node_t* get_node_from_index(
	ecs_map_t* map,
	uint32_t index)
{
	return ecs_vector_get(map->nodes, &(map->node_params), index - 1);
}

static ecs_map_node_t* get_node(
	ecs_map_t* map,
	bucket_t* bucket,
	uint64_t key)
{
	ecs_map_node_t* node = get_node_from_index(map, *bucket);

	// Loop forever until return only if node != NULL
	while (node)
	{
		if (node->key == key)
			return node;
		else
		{
			node = get_node_from_index(map, node->next);
		}
	}
	return NULL;
}

static void* get_node_data(
	ecs_map_node_t* node)
{
	return ECS_OFFSET(node, sizeof(ecs_map_node_t));
}

static void set_node_data(
	ecs_map_t* map,
	ecs_map_node_t* node,
	const void* data)
{
	if (!data) return;

	void* buffer = get_node_data(node);

	if (data != buffer)
	{
		memcpy(buffer, data, data_size(map));
	}
}

static void* add_node(
	ecs_map_t* map,
	bucket_t* bucket,
	uint64_t key,
	const void* data,
	ecs_map_node_t* prev_node)
{
	uint32_t index = 0;
	ecs_map_node_t* node;
	if (prev_node)
	{
		index = ecs_vector_get_index(map->nodes, &(map->node_params), prev_node);
		node = ecs_vector_get(map->nodes, &(map->node_params), index);
	}
	else
	{
		node = ecs_vector_addi(&(map->nodes), &(map->node_params), &index);
	}
	index++;

	node->key = key;
	node->prev = 0;
	node->next = 0;
	set_node_data(map, node, data);

	// If this bucket already has something in it
	uint32_t first = *bucket;
	if (first)
	{
		ecs_map_node_t* first_ptr = get_node_from_index(map, first);
		first_ptr->prev = index;
		node->next = first;
	}

	// The following is equivalent to 'map->buckets[key] = index;'
	*bucket = index;

	map->count++;
	
	return node;
}

static void resize_map(
	ecs_map_t* map,
	uint32_t bucket_count)
{
	bucket_t* old_buckets = map->buckets;
	uint32_t old_bucket_count = map->bucket_count;

	allocate_buckets(map, bucket_count);
	map->count = 0;

	for (uint32_t i = 0; i < old_bucket_count; ++i)
	{
		bucket_t bucket = old_buckets[i];
		if (bucket)
		{
			uint32_t node_index = bucket;
			uint32_t next;
			uint64_t key;

			ecs_map_node_t* node;
			do
			{
				node = get_node_from_index(map, node_index);
				next = node->next;
				key = node->key;
				
				bucket_t* new_bucket = get_bucket(map, key);
				add_node(map, new_bucket, key, get_node_data(node), node);
			} while (node_index == next);
		}
	}
	
	ecs_os_free(old_buckets);
}

ecs_map_t* ecs_map_new(
	uint32_t num_buckets,
	uint32_t element_size)
{
	ecs_map_t* result = ecs_os_malloc(sizeof(ecs_map_t));
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);

	result->count = 0;
	result->node_params = (ecs_vector_params_t){ .element_size = element_size + sizeof(ecs_map_node_t) };
	result->nodes = ecs_vector_new(&result->node_params, MAP_INITIAL_NODE_COUNT);

	allocate_buckets(result, num_buckets);

	return result;
}

void ecs_map_free(
	ecs_map_t* map)
{
	ecs_vector_free(map->nodes);
	ecs_os_free(map->buckets);
	ecs_os_free(map);
}

void* _ecs_map_set(
	ecs_map_t* map,
	uint64_t key,
	const void* data,
	size_t size)
{
	ecs_assert(data_size(map) == size, ECS_INVALID_PARAM, NULL);

	if (((float)map->count / (float)map->bucket_count) > MAP_LOAD_FACTOR)
	{
		resize_map(map, map->bucket_count * 2);
	}

	// Attempt to get the bucket at this index
	bucket_t* bucket = get_bucket(map, key);
	ecs_map_node_t* node = NULL;

	if (!*bucket)
	{
		// There is nothing in the bucket with this index
		node = add_node(map, bucket, key, data, NULL);
	}
	else
	{
		// There already exist some nodes in this bucket
		node = get_node(map, bucket, key);
		if (node)
		{
			// There is a node with this key in this bucket, just set data
			set_node_data(map, node, data);
		}
		else
		{
			node = add_node(map, bucket, key, data, NULL);
			node->next = *bucket - 1;
		}
	}

	ecs_assert(node != NULL, ECS_INTERNAL_ERROR, NULL);

	return get_node_data(node);
}

bool _ecs_map_has(
	ecs_map_t* map,
	uint64_t key_hash,
	void* data_out,
	size_t size)
{
	ecs_assert(data_size(map) == size, ECS_INVALID_PARAM, NULL);
	ecs_assert(data_out != NULL, ECS_INVALID_PARAM, NULL);

	if (map == NULL) return false;
	if (map->count == 0) return false;

	bucket_t* bucket = get_bucket(map, key_hash);
	if (*bucket)	// If there is something in the bucket associated with the key_hash
	{
		ecs_map_node_t* node = get_node(map, bucket, key_hash);

		if (node)
		{
			if (data_out)
				memcpy(data_out, get_node_data(node), data_size(map));
			return true;
		}
	}
	return false;
}

void* ecs_map_get_data_ptr(
	ecs_map_t* map,
	uint64_t key_hash)
{
	if (map == NULL) return NULL;
	if (map->count == 0) return NULL;

	bucket_t* bucket = get_bucket(map, key_hash);
	if (*bucket)	// If there is something in the bucket associated with the key_hash
	{
		ecs_map_node_t* node = get_node(map, bucket, key_hash);

		if (node)
		{
			return get_node_data(node);
		}
	}
	return NULL;
}

bool ecs_map_remove(
	ecs_map_t* map,
	uint64_t key)
{
	if (map->count == 0) return false;

	bucket_t* bucket = get_bucket(map, key);

	// If there is a bucket with this key...
	if (*bucket)
	{
		ecs_map_node_t* node = get_node(map, bucket, key);
		
		if (node != NULL)
		{
			ecs_map_node_t* prev_node = get_node_from_index(map, node->prev);
			ecs_map_node_t* next_node = get_node_from_index(map, node->next);

			if (prev_node != NULL)
			{
				ecs_assert(get_node_from_index(map, prev_node->next) == node, ECS_INTERNAL_ERROR, 0);
				prev_node->next = node->next;
			}
			else
			{
				*bucket = node->next;
			}

			if (next_node != NULL)
			{
				ecs_assert(get_node_from_index(map, next_node->prev) == node, ECS_INTERNAL_ERROR, 0);
				next_node->prev = node->prev;
			}

			// With ECS_VECTOR_REMOVE_SWAP_LAST, the last element will be moved to the
			// position of the removed element. Thus, the index of the last node will
			// be changed to the index of the removed node.
			ecs_vector_params_t params = map->node_params;
			params.move_flag = ECS_VECTOR_REMOVE_SWAP_LAST;

			ecs_map_node_t* move_node = ecs_vector_last(map->nodes, &params);
			ecs_map_node_t* move_prev_node = get_node_from_index(map, move_node->prev);
			uint32_t index = ecs_vector_get_index(map->nodes, &params, node);

			// If there is a node that comes before it, must update it's 'next' with the new index
			if (move_prev_node)
			{
				move_prev_node->next = index;
			}
			else
			{
				// If there is no previous, it is the "leader" of the bucket so get the bucket
				// ptr and update its value to point to the new index.
				bucket_t* bucket_move = get_bucket(map, move_node->key);
				*bucket_move = index + 1;	// make sure to apply an offset for this
			}
			ecs_vector_remove(map->nodes, &params, index);

			map->count--;
			return true;
		}
	}
	return false;
}

ecs_map_iter_t ecs_map_get_iter(
	ecs_map_t* map)
{
	ecs_map_iter_t result = {
		.map = map,
		.bucket_index = -1,
		.node_index = 0 };
	return result;
}

static
uint32_t next_bucket(
	ecs_map_t* map,
	uint32_t start_index)
{
	uint32_t i;
	for (i = start_index; i < map->bucket_count; ++i)
	{
		if (map->buckets[i])
			break;
	}

	return i;
}

bool ecs_map_has_next(
	ecs_map_iter_t* it)
{
	ecs_map_t* map = it->map;
	if (map->count == 0) return false;

	if (map->buckets == NULL) return false;

	uint32_t bucket_index = it->bucket_index;
	uint32_t node_index = it->node_index;

	if (node_index)
	{
		ecs_map_node_t* node = get_node_from_index(map, node_index);
		node_index = node->next;
	}

	if (!node_index)
	{
		bucket_index = next_bucket(map, bucket_index + 1);
		if (bucket_index < map->bucket_count)
		{
			node_index = map->buckets[bucket_index];
		}
		else
		{
			node_index = 0;
		}
	}

	if (node_index)
	{
		it->node_index = node_index;
		it->bucket_index = bucket_index;
		return true;
	}
	else
	{
		return false;
	}
}

void* ecs_map_next(
	ecs_map_iter_t* it)
{
	ecs_map_t* map = it->map;

	ecs_map_node_t* node = get_node_from_index(map, it->node_index);
	ecs_assert(node != NULL, ECS_INTERNAL_ERROR, 0);


	return get_node_data(node);
}

size_t ecs_map_data_size(
	ecs_map_t* map)
{
	return data_size(map);
}

uint32_t ecs_map_count(
	ecs_map_t* map)
{
	return map->count;
}

uint32_t ecs_map_bucket_count(
	ecs_map_t* map)
{
	return map->bucket_count;
}

uint64_t ecs_map_string_hash(
	const char* s,
	uint64_t seed)
{
	uint64_t hash = seed;
	while (*s)
	{
		hash = hash * 101 + *s++;
	}
	return hash;
}