#ifndef PRIVATE_TYPES_H
#define PRIVATE_TYPES_H

// Library includes
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>

#include <cecs.h>

/**
 * A system is essentially a function which runs under a certain context with
 * specific parameters. Systems may operate on specific types of components
 * depending on their purpose. In essence, a system performs operations on
 * components.
 *
 * For example: a system could run on every component of type 'A' and apply an
 * operation on the data stored in all components of that type.
 **/
typedef struct EcsSystem
{
	ecs_system_function_t function;	/* Function that is called for each element the system is run on. */
	EcsSystemType type;				/* The type of system. */
	ecs_system_status_t status;		/* Current status of the system. */
	double time_spent;				/* The time the system took to run. */
} EcsSystem;

/**
 * Stores information about where an entity is stored (which table) as well as
 * it's index in that table.
 **/
typedef struct ecs_row_t
{
	ecs_archetype_t archetype;	/* The indentifier of the table. */
	uint32_t index;				/* Index in its archetype table. */
} ecs_row_t;

/**
 * Columns are the objects which store the component data of a given 
 * component type for a given archetype.
 **/
typedef struct ecs_table_column_t 
{
	ecs_vector_t* data; /* Actual column data */
	uint16_t size;		/* Store this to save function calls */
} ecs_table_column_t;

/**
 *
 **/
typedef struct ecs_table_t
{
	ecs_table_column_t* columns;
	ecs_archetype_t archetype;
} ecs_table_t;

/**
 * A world stores and manages all data. This is effectively similar to the idea
 * of levels in UE4 or similar engines. Multiple worlds may be present and data
 * is never shared between them. 
 *
 * This is useful for things like context switching in case of the user opening
 * a menu or loading into a new level. The current active world can be swapped
 * whenever necessary.
 **/
typedef struct ecs_world_t
{
	/* Store information about entities: */

	ecs_map_t* entity_index;	/* Lookup table that maps <ecs_entity_t, ecs_row_t> */

	/* Store information about components and entities in tables. */

	ecs_vector_t* tables;		/* Stores pointers to all the entity tables. 
								   @TODO: memeory should be managed better on this. */
	
	ecs_map_t* table_index;		/* Lookup table by archetype. <archtype string hash, index into vector above> */

	/* Store information about systems: */

	ecs_vector_t* on_start_systems;
	ecs_vector_t* pre_update_systems;
	ecs_vector_t* on_update_systems;
	ecs_vector_t* post_update_systems;
	ecs_vector_t* on_quit_systems;
	ecs_vector_t* manual_systems;

	/* State data: */

	bool is_running;
	bool should_quit;		/* If this is set to true, the application will 
							   terminate on next loop. */

	ecs_entity_t last_id;	/* Stores the last given entity id. */
} ecs_world_t;

/* Some params for common array types. */
extern const ecs_vector_params_t archetype_id_params;
extern const ecs_vector_params_t table_params;
extern const ecs_vector_params_t handle_params;
extern const ecs_vector_params_t entity_handle_params;
#endif
