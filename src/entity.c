#include "cecs_private.h"

// Defining comparator function as per the requirement 
static int compare(const void* a, const void* b)
{
	// setting up rules for comparison 
	return strcmp(*(const char**)a, *(const char**)b);
}

// Function to sort the array 
static void sort(char* arr[], int n)
{
	// calling qsort function to sort the array 
	// with the help of Comparator 
	qsort(arr, n, sizeof(const char*), compare);
}

static void ecs_parse_arch_expr(
	const char* expr,
	ecs_vector_t** vec_out)
{
	size_t len = strlen(expr);
	if (len == 0) return;

	ecs_vector_t* result = *vec_out;

	// Create a temp buffer to store types while operating
	char* buffer = ecs_os_malloc(len * sizeof(char) + 1);
	uint32_t j = 0;

	// Increase by 1 more than len to be able to identify the string
	// terminator '\0'.
	for (uint32_t i = 0; i < len + 1; ++i)
	{
		if (expr[i] == ',' || expr[i] == '\0')
		{
			// Type name is finished, reset and move on to the next one
			buffer[j++] = '\0';

			uint32_t proper_len = j;
			char* proper_buff = malloc(proper_len * sizeof(char));
			ecs_assert(proper_buff != NULL, ECS_INTERNAL_ERROR, 0);

			memcpy(proper_buff, buffer, proper_len * sizeof(char));

			char** a = ecs_vector_add(&result, &archetype_id_params);
			*a = proper_buff;

			j = 0;
		}
		else if (expr[i] == ' ')
		{
			// ignore spaces since they cannot appear in type names
		}
		else
		{
			// Parse the character normally
			buffer[j++] = expr[i];
		}
	}

	// Sort the types array alphabetically so that they can be inputted in any order
	// and will still point to the same table internally.

	uint32_t count = ecs_vector_count(result);
	char** arr = ecs_vector_first(result);
	
	sort(arr, count);

	*vec_out = result;
	ecs_os_free(buffer);
}

static void set_arch_hash(
	ecs_archetype_t* arch)
{
	arch->hash = 0;
	uint32_t count = ecs_vector_count(arch->types);
	char** arr = ecs_vector_first(arch->types);

	for (uint32_t i = 0; i < count; ++i)
	{
		arch->hash = ecs_map_string_hash(arr[i], arch->hash);
	}
}

ecs_archetype_t arch_from_expr(
	const char* expr)
{
	ecs_archetype_t result;
	result.types = ecs_vector_new(&archetype_id_params, 0);
	ecs_parse_arch_expr(expr, &(result.types));

	set_arch_hash(&result);
	
	return result;
}

ecs_entity_t ecs_new_entity(
	ecs_world_t* world,
	const char* expr)
{
	ecs_archetype_t arch = arch_from_expr(expr);

	uint32_t count = ecs_vector_count(arch.types);
	char** arr = ecs_vector_first(arch.types);
	
	// Before adding the entity, check to make sure all component types
	// are known to the engine first.
	for (uint32_t i = 0; i < count; ++i)
	{
		// If any of the components aren't known, return an invalid entity
		// and free the buffers associated with the type names.
		if (_ecs_component_is_known(arr[i]) == false)
		{
			for (uint32_t i = 0; i < count; ++i)
			{
				ecs_os_free(arr[i]);
			}
			ecs_vector_free(arch.types);
			return ECS_INVALID_ENTITY_HANDLE;
		}
	}

	ecs_entity_t result = ecs_add_entity(world, &arch);

	return result;
}