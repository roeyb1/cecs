#ifndef CECS_PRIVATE_H
#define CECS_PRIVATE_H

#include "types.h"

void ecs_table_init(
	ecs_world_t* world,
	ecs_table_t* table);

void ecs_table_free(
	ecs_table_t* table);

uint32_t ecs_table_insert(
	ecs_table_t* table,
	ecs_table_column_t* columns,
	ecs_entity_t entity);

uint32_t ecs_table_count(
	ecs_table_t* table);

ecs_component_info_t* ecs_get_component_info(
	ecs_world_t* world,
	ecs_archetype_t* arch,
	uint32_t index);

ecs_entity_t ecs_add_entity(
	ecs_world_t* world,
	ecs_archetype_t* arch);

ecs_archetype_t arch_from_expr(
	const char* expr);

void* ecs_table_get_data(
	ecs_table_t* table,
	uint32_t row_index,
	const char* id);

void ecs_reset_known_components();

#endif