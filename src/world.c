#include "cecs_private.h"

/* Set up the common global array parameters. */
const ecs_vector_params_t archetype_id_params = { .element_size = sizeof(const char*) };
const ecs_vector_params_t table_params = { .element_size = sizeof(ecs_table_t*) };
const ecs_vector_params_t handle_params = { .element_size = sizeof(void*) };
const ecs_vector_params_t entity_handle_params = { .element_size = sizeof(ecs_entity_t) };

/** Register the table with the world. **/
static void set_table(
	ecs_world_t* world,
	ecs_archetype_t* archetype,
	ecs_table_t* table)
{
	if (world->tables == NULL)
		world->tables = ecs_vector_new(&table_params, 0);

	uint32_t index = 0;
	ecs_table_t** data = ecs_vector_addi(&(world->tables), &table_params, &index);
	*data = table;
	ecs_map_set(world->table_index, archetype->hash, &index);
}

static ecs_table_t* get_table(
	ecs_world_t* world,
	uint64_t arch_hash)
{
	uint32_t index;
	if (world == NULL) return NULL;
	// If there is already a table registered in the world for this archetype, simply return it
	if (ecs_map_has(world->table_index, arch_hash, &index) == true)
		return *(ecs_table_t**)ecs_vector_get(world->tables, &table_params, index);
	else
		return NULL;
}

/** Create a new table from an archetype. **/
static ecs_table_t* table_new(
	ecs_world_t* world,
	ecs_archetype_t* archetype)
{
	ecs_table_t* result = get_table(world, archetype->hash);
	if (result != NULL) return result;

	result = ecs_os_malloc(sizeof(ecs_table_t));
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);

	result->archetype = *archetype;

	ecs_table_init(world, result);

	set_table(world, archetype, result);

	return result;
}

ecs_world_t* ecs_init()
{
	ecs_world_t* result = ecs_os_malloc(sizeof(ecs_world_t));
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);

	result->entity_index = ecs_map_new(4, sizeof(ecs_row_t));
	result->table_index = ecs_map_new(4, sizeof(uint32_t));

	result->tables = ecs_vector_new(&table_params, 0);
	result->on_start_systems = ecs_vector_new(&handle_params, 0);
	result->pre_update_systems = ecs_vector_new(&handle_params, 0);
	result->on_update_systems = ecs_vector_new(&handle_params, 0);
	result->post_update_systems = ecs_vector_new(&handle_params, 0);
	result->on_quit_systems = ecs_vector_new(&handle_params, 0);
	result->manual_systems = ecs_vector_new(&handle_params, 0);

	result->is_running = true;
	result->should_quit = false;

	result->last_id = 0;

	return result;
}

void ecs_fini(
	ecs_world_t* world)
{
	// Run the OnQuit systems before destroying the world object
	uint32_t on_quit_count = ecs_vector_count(world->on_quit_systems);
	EcsSystem** on_quit_arr = ecs_vector_first(world->on_quit_systems);
	for (uint32_t i = 0; i < on_quit_count; ++i)
	{
		on_quit_arr[i]->function(world, 0);
	}

	ecs_table_t** arr = ecs_vector_first(world->tables);
	for (uint32_t i = 0; i < ecs_vector_count(world->tables); ++i)
	{
		ecs_table_free(arr[i]);
	}

	ecs_map_iter_t it = ecs_map_get_iter(world->entity_index);

	while (ecs_map_has_next(&it))
	{
		ecs_row_t* row = ecs_map_next(&it);
		if (row->archetype.types)
		{
			// Free all the string buffers that this vector refers to and then
			// actually free the vector itself
			uint32_t count = ecs_vector_count(row->archetype.types);
			char** arr = ecs_vector_first(row->archetype.types);

			for (uint32_t i = 0; i < count; ++i)
			{
				ecs_os_free(arr[i]);
			}

			ecs_vector_free(row->archetype.types);
		}
	}

	ecs_map_free(world->entity_index);
	ecs_map_free(world->table_index);

	ecs_vector_free(world->tables);
	ecs_vector_free(world->on_start_systems);
	ecs_vector_free(world->pre_update_systems);

	EcsSystem** sys_arr = ecs_vector_first(world->on_update_systems);
	for (uint32_t i = 0; i < ecs_vector_count(world->on_update_systems); ++i)
	{
		ecs_os_free(sys_arr[i]);
	}

	ecs_vector_free(world->on_update_systems);
	ecs_vector_free(world->post_update_systems);
	ecs_vector_free(world->on_quit_systems);
	ecs_vector_free(world->manual_systems);

	ecs_reset_known_components();

	ecs_os_free(world);
}

bool ecs_world_update(
	ecs_world_t* world,
	float deltaTime)
{
	if (world->should_quit)
	{
		return false;
	}

	EcsSystem** sys_arr = ecs_vector_first(world->on_update_systems);
	for (uint32_t i = 0; i < ecs_vector_count(world->on_update_systems); ++i)
	{
		sys_arr[i]->function(world, deltaTime);
	}
	return true;
}

bool ecs_entity_lookup(
	ecs_world_t* world,
	ecs_entity_t id)
{
	ecs_row_t temp;
	return ecs_map_has(world->entity_index, id, &temp);
}

static uint32_t add_to_table_index(
	ecs_world_t* world,
	ecs_archetype_t* arch,
	ecs_entity_t handle)
{
	ecs_table_t* table = get_table(world, arch->hash);
	
	// If the table doesn't exist, make it
	if (table == NULL)
	{
		table = table_new(world, arch);
	}
	
	// Add a row to the table
	uint32_t index = ecs_table_insert(table, table->columns, handle);
	return index;
}

ecs_entity_t ecs_add_entity(
	ecs_world_t* world,
	ecs_archetype_t* arch)
{
	ecs_entity_t result = ++(world->last_id);
	uint32_t index = add_to_table_index(world, arch, result);

	ecs_row_t row = {
		.archetype = *arch,
		.index = index };

	ecs_map_set(world->entity_index, result, &row);

	return result;
}

static ecs_row_t* get_row_index(
	ecs_world_t* world,
	ecs_entity_t entity)
{
	ecs_assert(entity != ECS_INVALID_ENTITY_HANDLE, ECS_INVALID_PARAM, 0);

	ecs_row_t temp;

	if (ecs_map_has(world->entity_index, entity, &temp) == true)
		return ecs_map_get_data_ptr(world->entity_index, entity);
	else
		return NULL;
}

void* _ecs_get_component(
	ecs_world_t* world,
	ecs_entity_t entity,
	const char* type)
{
	ecs_assert(entity != ECS_INVALID_ENTITY_HANDLE, ECS_INVALID_PARAM, 0);

	ecs_row_t* row = get_row_index(world, entity);
	if (row == NULL) return NULL;

	ecs_table_t* table = get_table(world, row->archetype.hash);

	return ecs_table_get_data(table, row->index, type);
}

void* _ecs_get_column(
	ecs_world_t* world,
	uint32_t* count_out,
	const char* type,
	const char* arch_expr)
{
	ecs_archetype_t arch = arch_from_expr(arch_expr);

	ecs_table_t* table = get_table(world, arch.hash);

	// Free the arch data now since it's no longer needed
	uint32_t count = ecs_vector_count(arch.types);
	char** arr = ecs_vector_first(arch.types);
	for (uint32_t i = 0; i < count; ++i)
	{
		ecs_os_free(arr[i]);
	}

	ecs_vector_free(arch.types);
	*count_out = ecs_table_count(table);
	return ecs_table_get_data(table, 1, type);
}

void ecs_component_set(
	ecs_world_t* world,
	ecs_entity_t entity,
	const char* type,
	size_t size,
	const void* data)
{
	void* data_ptr = _ecs_get_component(world, entity, type);
	if (data_ptr == NULL) return;
	memcpy(data_ptr, data, size);
}

void ecs_world_should_quit(
	ecs_world_t* world)
{
	world->should_quit = true;
}