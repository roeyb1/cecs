#include "cecs_private.h"

static bool ecs_os_api_initialized = false;
static bool ecs_os_api_debug_enabled = false;

const ecs_os_api_t ecs_os_api;

/* Allow access to API only in local translation unit */
static ecs_os_api_t* _ecs_os_api = (ecs_os_api_t*)& ecs_os_api;

void ecs_os_set_api(ecs_os_api_t* os_api)
{
	if (!ecs_os_api_initialized)
	{
		*_ecs_os_api = *os_api;
		ecs_os_api_initialized = true;
	}
}

static void ecs_log(const char* fmt, va_list args)
{
	fprintf(stdout, "[LOG] ");
	vfprintf(stdout, fmt, args);
	fprintf(stdout, "\n");
}

static void ecs_log_error(const char* fmt, va_list args)
{
	fprintf(stderr, "[ERR] ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
}

static void ecs_log_debug(const char* fmt, va_list args)
{
	if (ecs_os_api_debug_enabled)
	{
		fprintf(stderr, "[DBG] ");
		vfprintf(stderr, fmt, args);
		fprintf(stderr, "\n");
	}
}

static void ecs_log_warning(const char* fmt, va_list args)
{
	fprintf(stderr, "[WARN] ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
}

void ecs_os_dbg(const char* fmt, ...)
{
#ifdef DEBUG
	va_list args;
	va_start(args, fmt);
	if (ecs_os_api.log_debug)
	{
		ecs_os_api.log_debug(fmt, args);
	}
	va_end(args);
#else
	(void)fmt;
#endif
}

void ecs_os_warn(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	if (ecs_os_api.log_warning)
	{
		ecs_os_api.log_warning(fmt, args);
	}
	va_end(args);
}

void ecs_os_log(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	if (ecs_os_api.log)
	{
		ecs_os_api.log(fmt, args);
	}
	va_end(args);
}

void ecs_os_err(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	if (ecs_os_api.log_error)
	{
		ecs_os_api.log_error(fmt, args);
	}
	va_end(args);
}

void ecs_os_enable_dbg(bool enable)
{
	ecs_os_api_debug_enabled = enable;
}

bool ecs_os_dbg_enabled(void)
{
	return ecs_os_api_debug_enabled;
}

void ecs_os_set_api_defaults(void)
{
	if (ecs_os_api_initialized)
	{
		return;
	}

	_ecs_os_api->malloc = malloc;
	_ecs_os_api->free = free;
	_ecs_os_api->realloc = realloc;
	_ecs_os_api->calloc = calloc;

	// TODO: Set threading, mutex, condition variables

	_ecs_os_api->log = ecs_log;
	_ecs_os_api->log_error = ecs_log_error;
	_ecs_os_api->log_debug = ecs_log_debug;
	_ecs_os_api->log_warning = ecs_log_warning;

	_ecs_os_api->abort = abort;
}