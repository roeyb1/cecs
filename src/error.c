#include "cecs_private.h"

const char* ecs_strerror(
	uint32_t error_code)
{
	switch (error_code)
	{
	case ECS_OUT_OF_MEMORY:
		return "out of memory";
	case ECS_INTERNAL_ERROR:
		return "internal error";
	case ECS_INVALID_PARAM:
		return "invalid parameter";
	}

	return "unknown error code";
}

void _ecs_assert(
	bool condition,
	uint32_t error_code,
	const char* param,
	const char* condition_str,
	const char* file,
	uint32_t line)
{
	if (!condition) {
		if (param) {
			ecs_os_err("assert(%s) %s:%d: %s (%s)",
				condition_str, file, line, ecs_strerror(error_code), param);
		}
		else {
			ecs_os_err("assert(%s) %s:%d: %s",
				condition_str, file, line, ecs_strerror(error_code));
		}
		ecs_os_abort();
	}
}