#include "cecs_private.h"

/** Create a bunch of columns for storing each component type. **/
static ecs_table_column_t* new_cols(
	ecs_world_t* world,
	ecs_table_t* table,
	ecs_archetype_t* archetype)
{
	uint32_t count = ecs_vector_count(archetype->types);

	// reserve space for the number of types in the vector. Initialize to 0 with calloc 
	// so none types can be easily determined.
	ecs_table_column_t* result = ecs_os_calloc((uint64_t)count + 1, sizeof(ecs_table_column_t));
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);

	// First column is reserved for entity ids
	result[0].size = sizeof(ecs_entity_t);
	result[0].data = ecs_vector_new(&entity_handle_params, 0);

	for (uint32_t i = 0; i < count; ++i)
	{
		ecs_component_info_t* component = ecs_get_component_info(world, archetype, i);

		if (component) // check against NULL ptrs
		{
			result[i + 1].size = (uint16_t)component->size;
			ecs_vector_params_t params = { .element_size = (uint32_t)component->size };
			result[i + 1].data = ecs_vector_new(&params, 0);
		}
	}

	return result;
}

void ecs_table_init(
	ecs_world_t* world,
	ecs_table_t* table)
{
	table->columns = new_cols(world, table, &(table->archetype));
}

void ecs_table_free(
	ecs_table_t* table)
{
	uint32_t count = ecs_vector_count(table->archetype.types);
	// Free the columns for each component type AND the additional entity column
	for (uint32_t i = 0; i < count + 1; ++i)
	{
		ecs_vector_free(table->columns[i].data);
	}

	ecs_os_free(table->columns);
	ecs_os_free(table);
}

uint32_t ecs_table_insert(
	ecs_table_t * table,
	ecs_table_column_t* columns,
	ecs_entity_t entity)
{
	ecs_entity_t* e = ecs_vector_add(&(columns[0].data), &handle_params);
	ecs_assert(e != NULL, ECS_INTERNAL_ERROR, 0);

	*e = entity;

	uint32_t column_count = ecs_vector_count(table->archetype.types);

	// Resize all the column vectors
	for (uint32_t i = 1; i < column_count + 1; ++i)
	{
		uint32_t size = columns[i].size;
		if (size != 0)
		{
			ecs_vector_params_t params = { .element_size = size };
			ecs_vector_add(&(columns[i].data), &params);
		}
	}

	return ecs_vector_count(columns[0].data);
}

/** Get the column index for a component identifier **/
static int32_t get_column_index(
	ecs_table_t* table,
	const char* id)
{
	uint32_t count = ecs_vector_count(table->archetype.types);

	char** arr = ecs_vector_first(table->archetype.types);

	for (uint32_t i = 0; i < count; ++i)
	{
		if (strcmp(arr[i], id) == 0)
		{
			return i + 1;
		}
	}
	return -1;
}

void* ecs_table_get_data(
	ecs_table_t* table,
	uint32_t row_index,
	const char* id)
{
	ecs_assert(table != NULL, ECS_INVALID_PARAM, 0);
	int32_t column_index = get_column_index(table, id);
	
	if (column_index == -1) return NULL;

	uint32_t size = table->columns[column_index].size;
	ecs_vector_params_t params = { .element_size = size };

	return ecs_vector_get(table->columns[column_index].data, &params, row_index - 1);
}

uint32_t ecs_table_count(
	ecs_table_t* table)
{
	return ecs_vector_count(table->columns[0].data);
}