#include "cecs_private.h"

#define VECTOR_BUFFER(vector) ECS_OFFSET(vector, sizeof(ecs_vector_t))

struct ecs_vector_t
{
	uint32_t count;
	uint32_t size;
};

static ecs_vector_t* resize(
	ecs_vector_t* vector,
	uint32_t size)
{
	ecs_vector_t* result = ecs_os_realloc(vector, sizeof(ecs_vector_t) + size);
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);
	return result;
}

ecs_vector_t* ecs_vector_new(
	const ecs_vector_params_t* params,
	uint32_t num_elements)
{
	ecs_vector_t* result = ecs_os_malloc(sizeof(ecs_vector_t) + (uint64_t)num_elements * (uint64_t)params->element_size);
	ecs_assert(result != NULL, ECS_OUT_OF_MEMORY, 0);

	result->count = 0;
	result->size = num_elements;

	return result;
}

void ecs_vector_free(
	ecs_vector_t* vector)
{
	ecs_os_free(vector);
}

void* ecs_vector_addn(
	ecs_vector_t** array_inout,
	const ecs_vector_params_t* params,
	uint32_t num_elements)
{
	ecs_vector_t* result = *array_inout;

	if (!result)
	{
		result = ecs_vector_new(params, 1);
	}

	uint32_t old_count = result->count;
	uint32_t new_count = old_count + num_elements;
	uint32_t size = result->size;
	uint32_t element_size = params->element_size;

	// Don't need to resize, simply increment and return data pointer
	if (new_count <= size)
	{
		result->count = new_count;
	}
	// Need more space, allocate double the size of the old vector
	else
	{
		uint32_t new_size = 2 * size;

		// If just doubling isn't enough, add the required size. 
		// This can happen if adding more than current size of vector:
		if (new_size < num_elements)
		{
			new_size += num_elements;
		}
		
		result = resize(result, new_size * element_size);
		result->count = new_count;
		result->size = new_size;
	}


	*array_inout = result;

	return ECS_OFFSET(VECTOR_BUFFER(result), (uint64_t)old_count * (uint64_t)element_size);
}

void* ecs_vector_add(
	ecs_vector_t** array_inout,
	const ecs_vector_params_t* params)
{
	return ecs_vector_addn(array_inout, params, 1);
}


void* ecs_vector_addi(
	ecs_vector_t** array_inout,
	const ecs_vector_params_t* params,
	uint32_t* index_out)
{
	if (index_out != NULL) *index_out = (*array_inout)->count;
	return ecs_vector_addn(array_inout, params, 1);
}

void ecs_vector_remove(
	ecs_vector_t* vector,
	const ecs_vector_params_t* params,
	uint32_t index)
{
	// Trying to delete an index which doesn't exist in the vector
	if (index >= vector->count) return;

	// Should order be maintained?
	if (params->move_flag == ECS_VECTOR_REMOVE_ORDERED)
	{
		// Initialize a char pointer to store data that needs to be moved around
		char* current_element;

		for (uint32_t i = index; i < vector->count; i++)
		{
			current_element = ecs_vector_get(vector, params, i);
			uint32_t offset = params->element_size;
			memcpy(current_element, (char*)((uintptr_t)current_element + (uintptr_t)params->element_size), params->element_size);
		}
	}
	else if (params->move_flag == ECS_VECTOR_REMOVE_SWAP_LAST)
	{
		// Just swap the last element with the removed element
		// Faster but doesn't maintain order
		char* current_element = ecs_vector_get(vector, params, index);
		memcpy(current_element, ecs_vector_last(vector, params), params->element_size);
	}
	vector->count--;
}

void* ecs_vector_get(
	const ecs_vector_t* vector,
	const ecs_vector_params_t* params,
	uint32_t index)
{
	// If the index is greater than the number of elements, return NULL
	if (index >= vector->count)
	{
		//return ECS_OFFSET(VECTOR_BUFFER(vector), (vector->count - 1) * params->element_size);
		return NULL;
	}
	return ECS_OFFSET(VECTOR_BUFFER(vector), (uint64_t)index * (uint64_t)params->element_size);
}

void* ecs_vector_first(
	const ecs_vector_t* vector)
{
	return VECTOR_BUFFER(vector);
}

void* ecs_vector_last(
	const ecs_vector_t* vector,
	const ecs_vector_params_t* params)
{
	return ECS_OFFSET(VECTOR_BUFFER(vector), (uint64_t)(vector->count - 1) * (uint64_t)params->element_size);
}

void ecs_vector_clear(
	ecs_vector_t* vector)
{
	vector->count = 0;
}

uint32_t ecs_vector_size(
	const ecs_vector_t* vector)
{
	return vector->size;
}

uint32_t ecs_vector_count(
	const ecs_vector_t* vector)
{
	return vector->count;
}

uint32_t ecs_vector_get_index(
	const ecs_vector_t* vector,
	const ecs_vector_params_t* params,
	void* element)
{
	ecs_assert(element != NULL, ECS_INVALID_PARAM, NULL);
	ecs_assert(vector->size >= vector->count, ECS_INTERNAL_ERROR, NULL);

	uint32_t element_size = params->element_size;
	void* buffer = VECTOR_BUFFER(vector);
	return (uint32_t)(((char*)element - (char*)buffer) / element_size);
}