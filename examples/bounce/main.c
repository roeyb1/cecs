#include <cecs.h>
#include "bounceModule.h"

#define TICK (float)0.1

int main()
{
	ecs_os_set_api_defaults();
	ecs_world_t* world = ecs_init();
	
	ECS_IMPORT(world, BounceModule);

	// Create and initialize 5 balls
	for (uint32_t i = 0; i < 5; ++i)
	{
		ECS_ENTITY(world, Ball, BALL_ARCH);
		InitializeBall(world, Ball);
	}

	// Loop continuously while the world is alive
	while (ecs_world_update(world, TICK));


	ecs_fini(world);
	ecs_os_log("Bounce simulation terminated");
}