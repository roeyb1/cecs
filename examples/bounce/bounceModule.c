#include "bounceModule.h"
#include <math.h>

#define PI 3.141592654
#define G 9.8
#define K 0.0016
#define ETHR 0.01

#define Vix 6.94
#define Viy 39.4

void InitializeBall(ecs_world_t* world, ecs_entity_t ball)
{
	ECS_SET(world, ball, Position, { .x = 0, .y = 0 });
	// Initial velocity of 40m/s at 80 degrees
	ECS_SET(world, ball, Velocity, { .x = Vix, .y = Viy });
	ECS_SET(world, ball, Energy, { .x = 0.5 * Vix * Vix, .y = 0.5 * Viy * Viy });

	double TerminalVelocity = G / (4 * PI * K);
	ECS_SET(world, ball, StaticBallInfo, { 
		.mass = 1, 
		.radius = 1, 
		.vt = TerminalVelocity,
		.finished = false });
}

void BounceSystem(ecs_world_t* world, float dt)
{
	static double time = 0;
	uint32_t num_finished_balls = 0;

	ECS_COLUMN(world, Position, p_arr, BALL_ARCH);
	ECS_COLUMN(world, Velocity, v_arr, BALL_ARCH);
	ECS_COLUMN(world, StaticBallInfo, bi_arr, BALL_ARCH);

	for (uint32_t i = 0; i < p_arr_count; ++i)
	{
		if (bi_arr[i].finished == true)
		{
			// do nothing
		}
		else if (p_arr[i].y >= 0)
		{
			double factor = 1 - exp(-G * time / bi_arr[i].vt);
			p_arr[i].x = Vix * bi_arr[i].vt / G * factor;
			p_arr[i].y = bi_arr[i].vt / G * (Viy + bi_arr[i].vt) * factor - bi_arr[i].vt * time;
		}
		
		if (p_arr[i].y < 0)
		{
			p_arr[i].y = 0;
			// For now don't try to bounce the ball, just stop when it hits y = 0

			// Check to see if sim is finished
			if (++num_finished_balls == p_arr_count)
			{
				ecs_world_should_quit(world);
			}
		}
		ecs_os_log("X: %.2f, Y: %.2f, time: %.2f", p_arr[i].x, p_arr[i].y, time);
	}
	time += dt;
}

void BounceModuleImport(ecs_world_t* world)
{
	ECS_COMPONENT(world, Position);
	ECS_COMPONENT(world, Velocity);
	ECS_COMPONENT(world, Energy);
	ECS_COMPONENT(world, StaticBallInfo);

	ECS_SYSTEM(world, BounceSystem, EcsOnUpdate);
}