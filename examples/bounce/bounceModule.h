#include <cecs.h>

typedef struct Vec2
{
	double x;
	double y;
} Vec2;

typedef struct StaticBallInfo
{
	float mass;		/* Mass of the ball */
	float radius;	/* Radius of the ball */
	double vt;		/* Terminal Velocity */
	bool finished;	/* Don't update the ball anymore */
} StaticBallInfo;

typedef Vec2 Position;
typedef Vec2 Velocity;
typedef Vec2 Energy;

#define BALL_ARCH Position, Velocity, Energy, StaticBallInfo

void InitializeBall(ecs_world_t* world, ecs_entity_t ball);
void BounceSystem(ecs_world_t* world, float dt);

void BounceModuleImport(ecs_world_t* world);