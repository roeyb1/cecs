#ifndef CECS_H
#define CECS_H

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>

/* Defines a custom bool type */
#ifndef __cplusplus
#undef bool
#undef true
#undef false
typedef char bool;
#define false 0
#define true !false
#endif

#if defined(_MSC_VER) || defined (__MINGW32__)
#define WINDOWS_ENABLED
#elif defined (__unix__)
#define UNIX_ENABLED
#elif defined (__APPLE__)
#define APPLE_ENABLED
#endif

#ifndef CECS_STATIC
	#if defined(WINDOWS_ENABLED)		// if compiling in windows or for windows
		#define CECS_EXPORT __declspec(dllexport)
	#elif defined(UNIX_ENABLED)			// compiling for unix
		#define CECS_EXPORT __attribute__((__visibility__("default")))
	#else
		#pragma warning "Unknown dynamic link semantics but compiling for dynamic link"
		#define CECS_EXPORT
	#endif
#else
	#define CECS_EXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** Custom assert function. **/
CECS_EXPORT void _ecs_assert(
	bool condition,
	uint32_t error_code,
	const char* param,
	const char* condition_str,
	const char* file,
	uint32_t line);

#ifdef DEBUG
	#define ecs_assert(condition, error_code, param) _ecs_assert(condition, error_code, param, #condition, __FILE__, __LINE__)
#else
	#define ecs_assert(condition, error_code, param)
#endif

/* Error codes */
#define ECS_OUT_OF_MEMORY (1)
#define ECS_INTERNAL_ERROR (2)
#define ECS_INVALID_PARAM (3)

/* Important flags */
#define ECS_VECTOR_REMOVE_ORDERED (1)
#define ECS_VECTOR_REMOVE_SWAP_LAST (2)

#define ECS_INVALID_ENTITY_HANDLE (-1)

// Utility
#include <cecs/utils/vector.h>
#include <cecs/utils/map.h>
#include <cecs/utils/os_api.h>

/** --- Important Types --- **/

/** 
 * Archetypes are a collection of component types. This object stores component
 * identifiers. This vector is the template from which entities of a given 
 * archetype are created. 
 ***/
typedef struct ecs_archetype_t
{
	uint64_t hash;
	ecs_vector_t* types;	/* Vector of const char* */
} ecs_archetype_t;

/** Entities are just ids. **/
typedef int64_t ecs_entity_t;

typedef struct ecs_world_t ecs_world_t;

/** This typedef is used to simplify storing the component type sizes and identifiers. **/
typedef struct ecs_component_info_t
{
	const char* id;
	size_t size;
} ecs_component_info_t;

/* Function callback used by systems. */
typedef void (*ecs_system_function_t)(ecs_world_t*, float);

/* A list of the possible system types. */
typedef enum EcsSystemType
{
	EcsOnStart,		/* Runs when the application loads*/
	
	EcsPreUpdate,	/* Runs before the main update happens.*/
	EcsOnUpdate,	/* The main update. */
	EcsPostUpdate,	/* Runs after the main update. */

	EcsOnQuit,		/* Runs before the application closes. */

	EcsManual		/* System that must be run manually. */
} EcsSystemType;

typedef enum ecs_system_status_t
{
	EcsSystemNone,
	EcsSystemEnabled,
	EcsSystemDisabled,
} ecs_system_status_t;

/* --- World functions --- */

/**
 * Initialize a world object. The world object stores everything that happens
 * in the engine.
 *
 * @return A pointer to a new world object.
 **/
CECS_EXPORT
ecs_world_t* ecs_init();

CECS_EXPORT
void ecs_fini(
	ecs_world_t* world);

CECS_EXPORT
bool ecs_world_update(
	ecs_world_t* world,
	float deltaTime);

void ecs_world_should_quit(
	ecs_world_t* world);


#define ECS_IMPORT(world, mod) \
	mod##Import(world)

/* --- Component functions --- */

CECS_EXPORT
void ecs_new_component(
	ecs_world_t* world,
	const char* id,
	size_t size);

/**
 * Wrapper around ecs_new_component. Registers a component type with the
 * engine so that it can be used later when assigned to entities and/or
 * systems.
 **/
#define ECS_COMPONENT(world, id) \
	ecs_new_component(world, #id, sizeof(id))

CECS_EXPORT
void* _ecs_get_component(
	ecs_world_t* world,
	ecs_entity_t entity,
	const char* type);

#define ECS_GET_COMPONENT(world, entity, type, id) \
	type* id = (type*)_ecs_get_component(world, entity, #type)

CECS_EXPORT
void ecs_component_set(
	ecs_world_t* world,
	ecs_entity_t entity,
	const char* type,
	size_t size,
	const void* data);

#define ECS_SET(world, entity, type, ...) \
	ecs_component_set(world, entity, #type, sizeof(type), &(type)__VA_ARGS__)

bool _ecs_component_is_known(
	const char* id);

#define ecs_component_is_known(type) \
	_ecs_component_is_known(#type)

CECS_EXPORT
void* _ecs_get_column(
	ecs_world_t* world,
	uint32_t* count_out,
	const char* type,
	const char* arch_expr);

#define str(...) #__VA_ARGS__

#define ECS_COLUMN(world, type, id, ...) \
	uint32_t id##_count = 0; \
	type* id = (type*)_ecs_get_column(world, &id##_count, #type, str(__VA_ARGS__));
	



/* --- Entity functions --- */

ecs_entity_t ecs_new_entity(
	ecs_world_t* world,
	const char* expr);

#define ECS_ENTITY(world, id, ...) \
	ecs_entity_t id = ecs_new_entity(world, str(__VA_ARGS__))

bool ecs_entity_lookup(
	ecs_world_t* world,
	ecs_entity_t entity);



/* --- System functions --- */
	
void ecs_new_system(
	ecs_world_t* world,
	ecs_system_function_t func,
	EcsSystemType type);

#define ECS_SYSTEM(world, func, type) \
	ecs_new_system(world, func, type)

// Calculates offset from o to offset
#define ECS_OFFSET(o, offset) (void*)(((uintptr_t)(o)) + ((uintptr_t)(offset)))

#ifdef __cplusplus
}
#endif

#endif // CECS_H