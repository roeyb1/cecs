#ifndef CECS_VECTOR_H
#define CECS_VECTOR_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ecs_vector_t ecs_vector_t;

typedef struct ecs_vector_params_t
{
	uint32_t element_size;
	uint32_t move_flag;
} ecs_vector_params_t;

/**
 * Create a new vector using the given params.
 * @param params a pointer to the parameters with which to generate the vector.
 * @param num_elements the number of elements to preallocate in the vector.
 */
CECS_EXPORT
ecs_vector_t* ecs_vector_new(
	const ecs_vector_params_t* params,
	uint32_t num_elements);

/**
 * Frees all memory allocated to the vector.
 */
CECS_EXPORT
void ecs_vector_free(
	ecs_vector_t* vector);

/**
 * Add n elements to vector and returns a void pointer to start of the memeory block 
 * @param array_inout [in,out] a pointer to a vector pointer which will be updated 
 *					   with the new vector if resizing is necessary
 * @return a pointer to the start of the empty memory block.
 */
CECS_EXPORT
void* ecs_vector_addn(
	ecs_vector_t** array_inout,
	const ecs_vector_params_t* params,
	uint32_t num_elements);

/** 
 * Add a single element to vector and returns a void pointer to that location. 
 * @see ecs_vector_addn 
 */
CECS_EXPORT
void* ecs_vector_add(
	ecs_vector_t** array_inout,
	const ecs_vector_params_t* params);

/**
 * Add a single element to vector and returns a void pointer to that location.
 * @see ecs_vector_add
 * @param index_out [out] the value of index_out is set with the index of the element that was added
 */
CECS_EXPORT
void* ecs_vector_addi(
	ecs_vector_t** array_inout,
	const ecs_vector_params_t* params,
	uint32_t* index_out);

/**
 * Removes element at index.
 */
CECS_EXPORT
void ecs_vector_remove(
	ecs_vector_t* vector,
	const ecs_vector_params_t* params,
	uint32_t index);

/**
 * Returns a pointer to the element at index.
 */
CECS_EXPORT
void* ecs_vector_get(
	const ecs_vector_t* vector,
	const ecs_vector_params_t* params,
	uint32_t index);

/**
 * Returns a pointer to the first element of a vector.
 */
CECS_EXPORT
void* ecs_vector_first(
	const ecs_vector_t* vector);

/**
 * Returns a pointer to the last element of a vector.
 */
CECS_EXPORT
void* ecs_vector_last(
	const ecs_vector_t* vector,
	const ecs_vector_params_t* params);

/** 
 * Clear a vector of all values.
 */
CECS_EXPORT
void ecs_vector_clear(
	ecs_vector_t* vector);

/**
 * Returns the size of the block of memory allocated to a vector.
 */
CECS_EXPORT
uint32_t ecs_vector_size(
	const ecs_vector_t* vector);

/**
 * Returns the number of elements stored in a vector. This number is _not_ the size of
 * the elements in the vector.
 */
CECS_EXPORT
uint32_t ecs_vector_count(
	const ecs_vector_t* vector);

CECS_EXPORT
uint32_t ecs_vector_get_index(
	const ecs_vector_t* vector,
	const ecs_vector_params_t* params,
	void* element);

#ifdef __cplusplus
}
#endif

#endif