#ifndef CECS_MAP_H
#define CECS_MAP_H

#ifdef __cplusplus
extern "C" {
#endif


typedef struct ecs_map_t ecs_map_t;

typedef struct ecs_map_iter_t
{
	uint32_t bucket_index;
	uint32_t node_index;
	ecs_map_t* map;
} ecs_map_iter_t;

/**
 * Create a new map with n buckets. The map will be created to store elements
 * of size 'element_size'. 
 **/
CECS_EXPORT
ecs_map_t* ecs_map_new(
	uint32_t num_buckets,
	uint32_t element_size);

CECS_EXPORT
void ecs_map_free(
	ecs_map_t* map);

CECS_EXPORT
void* _ecs_map_set(
	ecs_map_t* map,
	uint64_t key,
	const void* data,
	size_t size);

/**
 * Set the value of an element in the map given a key. If the map doesn't
 * already have this key registered, create it and then set the corresponding
 * value.
 *
 * @return Returns a void* to the beginning of the data block
 * @warning do not attempt to dereference the data ptr if return value is false
 **/
#define ecs_map_set(map, key, data) \
	_ecs_map_set(map, key, data, sizeof(*data))

CECS_EXPORT
bool _ecs_map_has(
	ecs_map_t* map,
	uint64_t key_hash,
	void* data_out,
	size_t size);

/**
 * Check to see if the map has a given key and use the out parameter to return
 * a pointer to the corresponding location of the data if the key exists.
 *
 * @return Returns true if the map has a given key, false otherwise.
 **/
#define ecs_map_has(map, key_hash, data) \
	_ecs_map_has(map, key_hash, data, sizeof(*data))

/**
 * Returns a pointer to the start of the data block of a map node.
 **/
CECS_EXPORT
void* ecs_map_get_data_ptr(
	ecs_map_t* map,
	uint64_t key_hash);

CECS_EXPORT
ecs_map_iter_t ecs_map_get_iter(
	ecs_map_t* map);

CECS_EXPORT
bool ecs_map_has_next(
	ecs_map_iter_t* it);

CECS_EXPORT
void* ecs_map_next(
	ecs_map_iter_t* it);

/**
 * Attempts to remove an element from a map from a given key.
 *
 * @return Returns true if the operation was successful, false otherwise.
 **/
CECS_EXPORT
bool ecs_map_remove(
	ecs_map_t* map,
	uint64_t key);

CECS_EXPORT
size_t ecs_map_data_size(
	ecs_map_t* map);

CECS_EXPORT
uint32_t ecs_map_count(
	ecs_map_t* map);

CECS_EXPORT
uint32_t ecs_map_bucket_count(
	ecs_map_t* map);

CECS_EXPORT
uint64_t ecs_map_string_hash(
	const char* s,
	uint64_t seed);

#ifdef __cplusplus
}
#endif


#endif // CECS_MAP_H